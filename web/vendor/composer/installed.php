<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'eb37ccf1b07372de89c9d402eb531bac56aa0f92',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'eb37ccf1b07372de89c9d402eb531bac56aa0f92',
    ),
    'fpdf/fpdf' => 
    array (
      'pretty_version' => '1.83.2',
      'version' => '1.83.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '6aa31c9b70a3aef2a63f79144fec79b62bad4bb0',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '2.3.4',
      'version' => '2.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '437e7a1c50044b92773b361af77620efb76fff59',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0 || 2.0.0 || 3.0.0',
      ),
    ),
    'tecnickcom/tcpdf' => 
    array (
      'pretty_version' => '6.4.4',
      'version' => '6.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '42cd0f9786af7e5db4fcedaa66f717b0d0032320',
    ),
  ),
);
